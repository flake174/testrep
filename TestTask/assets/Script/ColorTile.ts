import {TileColors} from "./enums/TileColors"
import { TileState } from "./enums/TileState";
import Coords from "./Coords";

const {ccclass, property} = cc._decorator;

@ccclass
export default class ColorTile extends cc.Component {

    @property(cc.Sprite) sprite: cc.Sprite = null;

    @property(cc.SpriteFrame) blueTile: cc.SpriteFrame = null;
    
    @property(cc.SpriteFrame) greenTile: cc.SpriteFrame = null; 
    
    @property(cc.SpriteFrame) orangeTile: cc.SpriteFrame = null; 

    @property(cc.SpriteFrame) purpleTile: cc.SpriteFrame = null;
    
    @property(cc.SpriteFrame) redTile: cc.SpriteFrame = null; 

    @property(cc.SpriteFrame) yellowTile: cc.SpriteFrame = null; 
    
    public coords: Coords = null;

    private color: TileColors = null;

    private state: TileState = null;

    public getColor(): TileColors {
        return this.color;
    }

    public setColor(color: TileColors): void {
        this.color = color;
        this.setSprite(color);
    }

    public getState(): TileState {
        return this.state;
    }

    public setState(state: TileState): void {
        this.state = state;
    }

    private setSprite(color: TileColors): void {
        let tileColors = {
            [TileColors.Blue] : this.blueTile,
            [TileColors.Green] : this.greenTile,
            [TileColors.Orange] : this.orangeTile,
            [TileColors.Purple] : this.purpleTile,
            [TileColors.Red] : this.redTile,
            [TileColors.Yellow] : this.yellowTile
        }
        this.sprite.spriteFrame = tileColors[color];
    }

    public moveTo(positionTo: cc.Vec2): void {
        this.node.runAction(cc.moveTo(0.2, positionTo));
    }

    public moveDown(distance: number): void {
        this.node.runAction(cc.moveBy(0.5, 0, -distance));
    }

    public moveDiagonal(distance: number): void {
        this.node.runAction(cc.moveBy(0.5, distance, -distance));
    }

    public moveDiagonalLeft(distance: number): void {
        this.node.runAction(cc.moveBy(0.5, -distance, -distance));
    }
 
}
const {ccclass, property} = cc._decorator;

@ccclass
export default class QuitController extends cc.Component {

    onLoad () {
        this.node.on(cc.Node.EventType.MOUSE_UP, this.quitGame, this);
    }

    quitGame(): void {
        cc.game.end();
    }
}
export default class Coords {

    public row: number = null;
    public column: number = null;

    public constructor(row: number, column: number) {
        this.row = row;
        this.column = column;
    }

}
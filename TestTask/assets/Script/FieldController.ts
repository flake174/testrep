import Coords from "./Coords";
import {TileColors} from "./enums/TileColors";
import ColorTile from "./ColorTile";
import Booster from "./Booster";
import { BoosterType } from "./enums/BoosterType";
import ButtonController from "./ButtonController";

const {ccclass, property} = cc._decorator;

@ccclass
export default class FieldController extends cc.Component {

    @property(Number) rowNumber: number = 0;

    @property(Number) columnNumber: number = 0;

    @property(Number) tileSize: number = 0;

    @property(cc.SpriteFrame) spriteBlue: cc.SpriteFrame = null;

    @property(cc.Node) canvas: cc.Node = null;

    @property(cc.Prefab) tilePrefab: cc.Prefab = null;

    @property(cc.Prefab) boosterPrefab: cc.Prefab = null;

    @property(cc.Label) movesLabel: cc.Label = null;

    @property(cc.Label) missionLabel: cc.Label = null;

    private _field: ColorTile[][] = [];
    private _fieldOffset: cc.Vec2 = null;
    private _canvasWidth: number = null;
    private _canvasHeight: number = null;
    private _fieldWidth: number = null;
    private _fieldHeight: number = null;

    private static firstTile: ColorTile = null;

    private static secondTile: ColorTile = null;

    private static clickCount: number = 0;

    private static isFieldGenerated: boolean = false;

    private static generateFlag: number = 0;

    private static spawnCount: number = 0;

    private static isCombinationColumn: boolean = false;

    init(): void {
        this._fieldWidth = this.node.width;
        this._fieldHeight = this.node.height;

        this._canvasWidth = this.canvas.width;
        this._canvasHeight = this.canvas.height;

        this._fieldOffset = new cc.Vec2();
        this._fieldOffset.x = (this._canvasWidth - this._fieldWidth)/2;
        this._fieldOffset.y = (this._canvasHeight - this._fieldHeight)/2;

    }

    onLoad() {
        this.init();

        let tile: ColorTile;
        let coords: Coords;
        for(let row = 0; row < this.rowNumber; row++) {
            this._field.push([]);            
            for(let column = 0; column < this.columnNumber; column ++) {

                if (row < 5 && column > 4) {
                    //cell.block = true;
                    continue;
                }

                coords = new Coords(row, column);
                tile = cc.instantiate(this.tilePrefab).getComponent(ColorTile);
                tile.setColor(this.getColor(Math.floor(Math.random() * 6)));
                tile.coords = coords;
                tile.node.setPosition(this.coordsToPosition(coords));
                tile.node.parent = this.node;
                this._field[row][column] = tile;

            }
        }
        this.node.on(cc.Node.EventType.MOUSE_UP, (e) => this.onMouseUp(e), this)

        this.checkRow();
    }
    
    mouseLocationToCoords(location: cc.Vec2): Coords {
        let row = Math.floor((location.y - this._fieldOffset.y) / this.tileSize);
        let column = Math.floor((location.x - this._fieldOffset.x) / this.tileSize);
        return new Coords(row, column);
    }

    dropClickStats(): void {
        FieldController.clickCount = 0;
        FieldController.firstTile = null;
        FieldController.secondTile = null;
    }

    onMouseUp(e: cc.Event.EventMouse): void {

        const coords = this.mouseLocationToCoords(e.getLocationInView());
        const activeTile = this.getTile(coords);

        if (coords.row < 5 && coords.column > 4) {
            //cell.block = true;
            return;
        }

        if (activeTile == null || activeTile.node == null ) {
            return;
        }

        if (ButtonController.isButtonPressed) {
            if (activeTile instanceof Booster) {
                if (activeTile.getType() === BoosterType.Horisontal) {
                    this.deleteRow(activeTile.coords.row);
                } else if (activeTile.getType() === BoosterType.Vertical) {
                    this.deleteColumn(activeTile.coords);
                }
            } else {
                if (activeTile.getColor() === TileColors.Red) {
                    this.missionLabel.node.emit('decrement');
                }
                activeTile.node.removeFromParent();
                activeTile.node = null;
                this.moveTiles();
            }

            ButtonController.isButtonPressed = false;
            this.dropClickStats();
            return;
        }
   

        FieldController.clickCount++;
        if (FieldController.clickCount % 2 === 1) {
            FieldController.firstTile = activeTile;
        }

        if (FieldController.clickCount % 2 === 0) {
            FieldController.secondTile = activeTile;

            if (FieldController.firstTile === FieldController.secondTile && activeTile instanceof Booster) {
                if (activeTile.getType() == BoosterType.Horisontal) {
                    this.deleteRow(activeTile.coords.row);
                }
                if (activeTile.getType() == BoosterType.Vertical) {
                    this.deleteColumn(activeTile.coords);
                }
                return;
            }

            if (this.isNeighbor(FieldController.firstTile, FieldController.secondTile)) {
                this.swap(FieldController.firstTile, FieldController.secondTile);
            } else {

                //FIX
                this.dropClickStats();
            }
        }

    }

    deleteRow(row: number): void {
        this._field[row].forEach(element => {
            if (element.node == null) {
                return;
            }

            if (element.getColor() === TileColors.Red && !(element instanceof Booster)) {
                this.missionLabel.node.emit('decrement');
            }
            element.node.removeFromParent();
            element.node = null;
        });

        this.moveTiles();
    }

    deleteColumn(coords: Coords): void {
        let columnSize: number;
        let column = coords.column;
        if (column < 5) {
            columnSize = 0;
        } else {
            columnSize = 5;
        }
        for(let row = columnSize; row < this.rowNumber; row++) {
            if (this._field[row][column].node == null) {
                continue;
            }

            if (this._field[row][column].getColor() === TileColors.Red && !(this._field[row][column] instanceof Booster)) {
                this.missionLabel.node.emit('decrement');
            }
            this._field[row][column].node.removeFromParent();
            this._field[row][column].node = null;
        }
        FieldController.isCombinationColumn = true;
        this.moveTiles();
    }
    
    getTile(coords: Coords): ColorTile {
        return this._field[coords.row][coords.column];
    }

    swap(firstTile: ColorTile, secondTile: ColorTile): void {
        this._field[firstTile.coords.row][firstTile.coords.column] = secondTile;
        this._field[secondTile.coords.row][secondTile.coords.column] = firstTile;
     
        let positionFirst = firstTile.node.getPosition();
        let positionSecond = secondTile.node.getPosition();

        firstTile.moveTo(positionSecond);
        secondTile.moveTo(positionFirst);
      
        firstTile.coords = this.positionToCoords(positionSecond);
        secondTile.coords = this.positionToCoords(positionFirst);

        this.movesLabel.node.emit('decrement');
        this.scheduleOnce(this.checkRow, 0.2);
    }

    isNeighbor (c1: ColorTile, c2: ColorTile): boolean {
        if (!cc.isValid(c1) || !cc.isValid(c2)) {
            return false;
        }
        let axisDifferenceX = Math.abs(c1.coords.column - c2.coords.column);
        let axisDifferenceY = Math.abs(c1.coords.row - c2.coords.row);
        if (axisDifferenceX > 1 || axisDifferenceY > 1) {
            return false;
        }

        return Boolean(axisDifferenceX ^ axisDifferenceY);
    }

    getColor(value: Number): TileColors {
        switch(value) {
            case 0:
                return TileColors.Blue;
            case 1:
                return TileColors.Green;
            case 2:
                return TileColors.Orange;
            case 3:
                return TileColors.Purple;
            case 4:
                return TileColors.Red;
            case 5:
                return TileColors.Yellow;
        }
    }

    positionToCoords(position: cc.Vec2): Coords {
        let row = (position.y + this.tileSize/2 - this._fieldHeight/2) / this.tileSize;
        let column = (position.x - this.tileSize/2 + this._fieldWidth/2) / this.tileSize;
        return new Coords(this.roundAbs(row), this.roundAbs(column));
    }

    coordsToPosition(coords: Coords): cc.Vec2 {
        let positionX = -this._canvasWidth/2 + this._fieldOffset.x + this.tileSize*(coords.column + 1) - this.tileSize/2;
        let positionY = this._canvasHeight/2 - this._fieldOffset.y - this.tileSize*(coords.row + 1) + this.tileSize/2;
        return new cc.Vec2(positionX, positionY);
    }

    roundAbs (value: number): number {
        return Math.round(Math.abs(value));
    }

    check(first: ColorTile, second: ColorTile, third: ColorTile, row: number, column: number, type: BoosterType): boolean {
        if (!(second instanceof Booster) && !(third instanceof Booster)) {
            if (this.compare(first, second) && this.compare(first, third)) {

                if (type === BoosterType.Horisontal) {
                    this.deleteRow(row);
                } else if (type === BoosterType.Vertical) {
                    this.deleteColumn(new Coords(row, column));
                }
                return true;
            }
        }
        return false;
    }

    checkBooster(row: number, column: number, type: BoosterType): boolean {
        let field = this._field;

        if (type === BoosterType.Horisontal) {
            if (column > 0 && column < this._field[row].length - 1) {
                if (this.check(field[row][column], field[row][column - 1], field[row][column + 1], row, column, type)) {
                    return true;
                }
            }
            if (column < this._field[row].length - 2) {
                if (this.check(field[row][column], field[row][column + 1], field[row][column + 2], row, column, type)) {
                    return true;
                }
            }
            if (column > 1) {
                if (this.check(field[row][column], field[row][column - 1], field[row][column - 2], row, column, type)) {
                    return true;
                }
            }
        }

        

        let minRow: number;
        if (column < 5) {
            minRow = 0;
        } else {
            minRow = 5;
        }

        if (type === BoosterType.Vertical) {
            if (row > minRow && row < this.rowNumber - 1) {
                if (this.check(field[row][column], field[row - 1][column], field[row + 1][column], row, column, type)) {
                    return true;
                }
            }
            if (row < this.rowNumber - 2) {
                if (this.check(field[row][column], field[row + 1][column], field[row + 2][column], row, column, type)) {
                    return true;
                }
            }
            if (row > minRow + 1) {
                if (this.check(field[row][column], field[row - 1][column], field[row - 2][column], row, column, type)) {
                    return true;
                }
            }
        }
        

        return false;
    }

    checkRow(): void {
        FieldController.isCombinationColumn = false;
        let field = this._field;
        let count = 0;
        FieldController.spawnCount = 1;
        //let tile: ColorTile;
        for (let row = 0; row < this.rowNumber; row++) {
            for (let column = 0; column < field[row].length; column++) {

                if (field[row][column] == null) {
                    count = 0;
                    continue;
                }

                if (field[row][column] instanceof Booster) {
                    let booster: Booster = field[row][column];                          //FIX
                    if (booster.getType() === BoosterType.Horisontal) {
                        if (this.checkBooster(row, column, BoosterType.Horisontal)) {
                            return;
                        }
                    } else if (booster.getType() === BoosterType.Vertical) {
                        if (this.checkBooster(row, column, BoosterType.Vertical)) {
                            return;
                        }
                    }
                    // if (this.checkBooster(row, column)) {
                    //     return;
                    // }
                }

                if (column !== 0 && field[row][column - 1].node != null) {
                    if (this.compare(field[row][column - 1], field[row][column]) && !(field[row][column - 1] instanceof Booster)
                    && !(field[row][column] instanceof Booster)) {
                        count++;
                    } else {
                        count = 0;
                    }
                }

                if (count === 2) {

                    if (FieldController.isFieldGenerated && column < field[row].length-1 && field[row][column+1].node !== null) {
                        if (this.compare(field[row][column], field[row][column + 1]) && !(field[row][column + 1] instanceof Booster)) {

                            let color = field[row][column].getColor();
                            let coords = new Coords(row, column + 1);

                            if (field[row][column+1].getColor() === TileColors.Red) {
                                this.missionLabel.node.emit('decrement');
                            }
                            field[row][column+1].node.destroy();
                            field[row][column+1] = this.createBooster(color, BoosterType.Horisontal, coords);

                           }
                       }

                       if (field[row][column].getColor() === TileColors.Red) {
                           this.missionLabel.node.emit('decrement3');
                       }

                    field[row][column].node.removeFromParent();
                    field[row][column - 1].node.removeFromParent();
                    field[row][column - 2].node.removeFromParent();

                    field[row][column].node = null;
                    field[row][column - 1].node = null;
                    field[row][column - 2].node = null;
                    count = 0;

                    if (!FieldController.isFieldGenerated) {
                        this.getNewTiles(row, column, true);
                        this.checkRow();
                        return;
                    }

                    this.moveTiles();
                    return;
                }
            }

            count = 0;
        }
        this.checkColumn();
    }

    checkColumn(): void {

        let field = this._field
        let count = 0;
        let columnSize = 0;
        for(let column = 0; column < this.columnNumber; column++) {
            if (column >= 5) {
                columnSize = 5; 
            }
            for(let row = columnSize; row < this.rowNumber; row++) {
                if (field[row][column].node == null) {
                    count = 0;
                    continue;
                }
                if (columnSize === 0 && row != 0 && field[row - 1][column].node != null ||
                    columnSize === 5 && row != 5 && field[row - 1][column].node != null) {
                    if (this.compare(field[row - 1][column], field[row][column]) && !(field[row - 1][column] instanceof Booster)
                    && !(field[row][column] instanceof Booster)) {
                        count++;
                    } else {
                        count = 0;
                    }
                }

                if (count === 2) {

                    if(FieldController.isFieldGenerated && row < this.rowNumber-1 && field[row+1][column] !== null) {
                        if (this.compare(field[row][column], field[row + 1][column]) && !(field[row + 1][column] instanceof Booster)) {

                            let color = field[row][column].getColor();
                            let coords = new Coords(row + 1, column);

                            if (field[row][column].getColor() === TileColors.Red) {
                                this.missionLabel.node.emit('decrement');
                            }

                            field[row + 1][column].node.destroy();
                            field[row + 1][column] = this.createBooster(color, BoosterType.Vertical, coords);  

                           }
                        }

                        if (field[row][column].getColor() === TileColors.Red) {
                            this.missionLabel.node.emit('decrement3');
                        }
                       
                    field[row][column].node.removeFromParent();
                    field[row - 1][column].node.removeFromParent();
                    field[row - 2][column].node.removeFromParent();
                    count = 0;

                    field[row][column].node = null;
                    field[row - 1][column].node = null;
                    field[row - 2][column].node = null;

                    if (!FieldController.isFieldGenerated) {
                        FieldController.generateFlag = 1;
                        this.getNewTiles(row, column, false);
                        this.checkColumn();
                        return;
                    }

                    FieldController.isCombinationColumn = true;

                    if (FieldController.isFieldGenerated) {
                        this.moveTiles();
                        return;
                    } 
                }
            }
            count = 0;
        }

        if (FieldController.generateFlag === 1 && !FieldController.isFieldGenerated) {
            FieldController.generateFlag = 0;
            this.checkRow();
        } else if (!FieldController.isFieldGenerated) {
            FieldController.isFieldGenerated = true;
            return;
        }
    
        if (FieldController.isFieldGenerated && FieldController.isCombinationColumn === true) {
           this.moveTiles();
        } 

    }

    createBooster(color: TileColors, type: BoosterType, coords: Coords): Booster {
        let tile: Booster;
        tile = cc.instantiate(this.boosterPrefab).getComponent(Booster);
        tile.setColor(color);
        tile.setType(type)
        tile.coords = coords;
        tile.node.setPosition(this.coordsToPosition(coords));
        tile.node.parent = this.node;
        return tile;
    }

    getNewTiles(row: number, column: number, isRow: boolean): void {
        let element: number;
        let tile: ColorTile;
        let coords: Coords;

        element = isRow ? column : row;

        for(let newElement = element; newElement >= element - 2; newElement--) {
            tile = cc.instantiate(this.tilePrefab).getComponent(ColorTile);
            tile.setColor(this.getColor(Math.floor(Math.random() * 6)));
            tile.node.parent = this.node;

            if (isRow) {
                coords = new Coords(row, newElement);
                this._field[row][newElement] = tile;                
            } else {
                coords = new Coords(newElement, column);
                this._field[newElement][column] = tile;                
            }
            tile.coords = coords;
            tile.node.setPosition(this.coordsToPosition(coords));
        }
    }

    moveTiles (): void {
        let field = this._field;
        let columnSize = 0;
        let tile: ColorTile;
        for (let column = 0; column < this.columnNumber; column++) {
            if ( column >= 5) {
                columnSize = 5;
            }
            for (let row = columnSize ; row < this.rowNumber; row++) {

                if (field[row][column].node == null) {

                    if (row === columnSize && columnSize === 0) {

                        tile = this.spawn(column, FieldController.spawnCount);
                        tile.moveDown(this.tileSize * FieldController.spawnCount);

                        if (FieldController.isCombinationColumn) {
                            FieldController.spawnCount++;
                        }
                        
                        this.moveTiles();
                        return;
                    }

                    if (row != columnSize && field[row - 1][column].node != null) {
                        field[row - 1][column].moveDown(this.tileSize);

                        let temp = field[row][column];
                        field[row][column] = field[row - 1][column];
                        field[row - 1][column] = temp;
                        
                        field[row][column].coords = new Coords(row, column);
                        field[row - 1][column].coords = new Coords(row - 1, column);

                        this.moveTiles();
                        return;
                    }

                }

                // if (row === column && row >= 4 && row !== this.rowNumber - 1) {
                //     if (field[row][column].node != null && field[row + 1][column + 1].node == null) {
                //         field[row][column].moveDiagonal(this.tileSize);

                //         let temp = field[row + 1][column + 1];
                //         field[row + 1][column + 1] = field[row][column];
                //         field[row][column] = temp;

                //         field[row + 1][column + 1].coords = new Coords(row + 1, column + 1);
                //         field[row][column].coords = new Coords(row, column);
                   
                //         this.moveTiles();
                //         return;
                //     }
                // }
                
            }
        }
        setTimeout(() => {
            this.checkRow();
        }, 500);
    }

    spawn(column: number, count: number): ColorTile {
        let coords = new Coords(0, column);
        let tile = cc.instantiate(this.tilePrefab).getComponent(ColorTile);
        tile.setColor(this.getColor(Math.floor(Math.random() * 6)));
        tile.coords = coords;
        tile.node.setPosition(this.getSpawnPosition(coords, count));
        tile.node.parent = this.node;
        this._field[0][column] = tile;
        return tile;
    }

    getSpawnPosition(coords: Coords, count: number): cc.Vec2 {
        let position = this.coordsToPosition(coords);
        return new cc.Vec2(position.x, position.y + this.tileSize * count);
    }

    compare(firstNode: ColorTile, secondNode: ColorTile): boolean {
        return firstNode.getColor() === secondNode.getColor();
    }
    
}
const {ccclass, property} = cc._decorator;

@ccclass
export default class ProgressBar extends cc.Component {

    @property(cc.ProgressBar) progressBar: cc.ProgressBar = null;

    private speed = 0.0125;

    onLoad() {
        this.updateProgressBar(this.progressBar);
    }

    updateProgressBar(progressBar: cc.ProgressBar) {
        let progress = progressBar.progress += this.speed;

        if (progress >= 1) {
            cc.director.loadScene("Game");
        }
   
        this.scheduleOnce(() => {
            this.updateProgressBar(progressBar);
        }, 0.025);
    }
}

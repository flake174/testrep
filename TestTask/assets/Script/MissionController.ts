const {ccclass, property} = cc._decorator;

@ccclass
export default class MissionController extends cc.Component {

    private static missionCount = 10;

    public static setMissionCount(node : number) {
        MissionController.missionCount = node;
    }

    public static getMissionCount(): number {
        return MissionController.missionCount;
    }

    public static decrement(): void {
        MissionController.missionCount--;
    }

    public static decrement3(): void {
        MissionController.missionCount -= 3;
    }

    @property(cc.Label)
    label: cc.Label = null;

    onLoad() {
        let label: cc.Label = this.label;
        this.label.string = MissionController.missionCount.toString();

        this.node.on('decrement', function () {
            MissionController.decrement();
            label.string = MissionController.missionCount.toString();
        });

        this.node.on('decrement3', function () {
            MissionController.decrement3();
            label.string = MissionController.missionCount.toString();
        });
    }

}

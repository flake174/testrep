import ColorTile from "./ColorTile";
import { BoosterType } from "./enums/BoosterType";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Booster extends ColorTile {

    @property(cc.SpriteFrame) lineVertical: cc.SpriteFrame = null; 

    @property(cc.SpriteFrame) lineHorisontal: cc.SpriteFrame = null; 

    private type: BoosterType = null;

    public setType(type: BoosterType) {
        let booster = {
            [BoosterType.Vertical] : this.lineVertical,
            [BoosterType.Horisontal] : this.lineHorisontal
        }
        let line = new cc.Node();
        let sprite = line.addComponent(cc.Sprite);
        sprite.spriteFrame = booster[type];
        line.parent = this.node;
        this.type = type;
    }

    public getType(): BoosterType {
        return this.type;
    }
}

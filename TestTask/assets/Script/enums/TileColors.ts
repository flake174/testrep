export enum TileColors {
    Blue = 'blue',
    Green = 'green',
    Red = 'red',
    Purple = 'purple',
    Yellow = 'yellow',
    Orange = 'orange'
}
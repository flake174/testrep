export enum TileState {
   Inactive = 'inactive',
   Moving = 'moving',
   Block = 'block',
   Spawn = 'spawn',
   Destruction = 'destruction'
   //бездействие, перемещение, блок, спавн, уничтожение
}
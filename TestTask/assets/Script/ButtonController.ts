const {ccclass, property} = cc._decorator;

@ccclass
export default class ButtonController extends cc.Component {

    public static isButtonPressed = false;

    onLoad() {
        this.node.on(cc.Node.EventType.MOUSE_UP, this.onMouseUp, this);
    }

    onMouseUp() {
        ButtonController.isButtonPressed = true;
    }

}

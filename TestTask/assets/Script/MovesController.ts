const {ccclass, property} = cc._decorator;

@ccclass
export default class MovesController extends cc.Component {

    private static movesCount: number = 12;

    public static setMovesCount(node : number) {
        MovesController.movesCount = node;
    }

    public static getMovesCount(): number {
        return MovesController.movesCount;
    }

    public static decrement(): void {
        MovesController.movesCount--;
    }

    @property(cc.Label)
    label: cc.Label = null;

    onLoad () {
        let label: cc.Label = this.label;
        this.label.string = MovesController.movesCount.toString();

        this.node.on('decrement', function () {
            MovesController.decrement();
            label.string = MovesController.movesCount.toString();
        });
    }

}
